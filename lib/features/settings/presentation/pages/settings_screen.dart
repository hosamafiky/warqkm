import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/enums/language.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../widgets/language_radio_option.dart';
import '../widgets/toggle_option.dart';

class SettingsScreen extends StatelessWidget {
  static const String routeName = '/settings';
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.settings_settings.tr())),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 12.aw, vertical: 32.ah),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              LocaleKeys.auth_dummy_text.tr(),
              maxLines: 2,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displayMedium!.copyWith(height: 1.5),
            ),
            30.vsb,
            Text(
              LocaleKeys.settings_lang_settings.tr(),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            23.vsb,
            Row(
              children: Language.values
                  .where((element) => context.supportedLocales.map((e) => e.languageCode).contains(element.code))
                  .map((locale) => Expanded(child: LanguageRadioOption(locale)))
                  .toList(),
            ),
            46.vsb,
            Text(
              LocaleKeys.settings_notif_settings.tr(),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            20.vsb,
            ToggleOption(
              title: LocaleKeys.settings_get_notifications.tr(),
              onTap: () {},
            ),
            24.vsb,
            ToggleOption(
              title: LocaleKeys.settings_offers_notifications.tr(),
              onTap: () {},
            ),
            24.vsb,
            ToggleOption(
              title: LocaleKeys.settings_following_store_notif.tr(),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}
