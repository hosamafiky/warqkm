import 'dart:math';

import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';

class ToggleOption extends StatelessWidget {
  final String title;
  final VoidCallback onTap;

  ToggleOption({super.key, required this.title, required this.onTap});

  final ValueNotifier<bool> valueNotifier = ValueNotifier(false);
  final ValueNotifier<int> rotateNotifier = ValueNotifier(0);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.bodySmall!.copyWith(color: LightAppColors.textColor2),
        ),
        ValueListenableBuilder(
          valueListenable: valueNotifier,
          builder: (context, bool value, child) {
            return InkWell(
              onTap: () {
                valueNotifier.value = !value;
                rotateNotifier.value == 0 ? rotateNotifier.value = 180 : rotateNotifier.value = 0;
              },
              child: Transform.rotate(
                angle: rotateNotifier.value * pi / 180,
                child: Image.asset(
                  'assets/images/toggle.png',
                  width: 38.5.aw,
                  height: 16.ah,
                  color: value ? Theme.of(context).primaryColor : const Color.fromRGBO(159, 159, 159, 0.4),
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
