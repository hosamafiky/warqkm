import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/enums/language.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';

class LanguageRadioOption extends StatelessWidget {
  const LanguageRadioOption(
    this.language, {
    super.key,
  });

  final Language language;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => context.setLocale(Locale(language.code)),
      child: Row(
        children: [
          Image.asset(
            'assets/images/${context.locale.languageCode == language.code ? 'radiobuttonon' : 'radio'}.png',
            width: 20.aw,
            height: 20.aw,
          ),
          9.hsb,
          Text(
            language.name,
            style: Theme.of(context).textTheme.bodySmall!.copyWith(color: LightAppColors.textColor2),
          ),
        ],
      ),
    );
  }
}
