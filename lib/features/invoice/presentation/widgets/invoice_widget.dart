import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../../../core/themes/light/light_colors.dart';
import '../../domain/entities/invoice.dart';

class InvoiceWidget extends StatelessWidget {
  const InvoiceWidget(this.invoice, {super.key});

  final Invoice invoice;

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7.asp),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(12.asp),
            height: 39.ah,
            color: LightAppColors.greyF5F5,
            child: Row(
              children: [
                Image.asset('assets/images/file.png', width: 15.aw, height: 15.aw),
                8.hsb,
                Text(
                  LocaleKeys.invoices_invoice_no.tr(),
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        height: 1.1,
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                      ),
                ),
                const Spacer(),
                Text(
                  '#${invoice.invoiceNo}',
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        height: 1.1,
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                      ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(12.aw),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      LocaleKeys.invoices_invoice_date.tr(),
                      style: Theme.of(context).textTheme.displayMedium!.copyWith(
                            height: 1.1,
                            color: const Color(0xFF777777),
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                    Text(
                      invoice.date,
                      style: Theme.of(context).textTheme.displayMedium!.copyWith(
                            height: 1.1,
                            color: Colors.black,
                            fontSize: 14.asp,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                  ],
                ),
                12.vsb,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      LocaleKeys.invoices_invoice_cost.tr(),
                      style: Theme.of(context).textTheme.displayMedium!.copyWith(
                            height: 1.1,
                            color: const Color(0xFF777777),
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                    Text(
                      '${invoice.cost} ${LocaleKeys.currency_min_short.tr()}',
                      style: Theme.of(context).textTheme.displaySmall!.copyWith(
                            height: 1.1,
                            fontSize: 14.asp,
                          ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              // Go To Invoice Details.
            },
            child: Container(
              height: 39.ah,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    LocaleKeys.invoices_show_invoice.tr(),
                    style: TextStyle(
                      fontSize: 13.asp,
                      fontFamily: 'URW',
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  14.hsb,
                  Container(
                    width: 16.aw,
                    height: 16.aw,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withOpacity(0.21),
                    ),
                    child: const FittedBox(
                      fit: BoxFit.contain,
                      child: Icon(
                        Icons.keyboard_arrow_left,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
