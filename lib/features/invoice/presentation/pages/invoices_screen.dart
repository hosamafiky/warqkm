import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/features/invoice/presentation/widgets/invoice_widget.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../domain/entities/invoice.dart';

class InvoicesScreen extends StatelessWidget {
  static const String routeName = '/invoices';
  const InvoicesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.invoices_invoices.tr())),
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (notification) {
          notification.disallowIndicator();
          return true;
        },
        child: ListView.separated(
          padding: EdgeInsets.symmetric(horizontal: 16.aw, vertical: 32.ah),
          shrinkWrap: true,
          itemBuilder: (context, index) => InvoiceWidget(invoices[index]),
          separatorBuilder: (c, i) => 20.vsb,
          itemCount: invoices.length,
        ),
      ),
    );
  }
}

List<Invoice> invoices = List.filled(
  6,
  const Invoice(invoiceNo: 12345, date: '10/3/2022', cost: 1500.000),
);
