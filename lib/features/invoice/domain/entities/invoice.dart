// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class Invoice extends Equatable {
  final int invoiceNo;
  final String date;
  final double cost;

  const Invoice({
    required this.invoiceNo,
    required this.date,
    required this.cost,
  });

  @override
  List<Object> get props => [invoiceNo, date, cost];
}
