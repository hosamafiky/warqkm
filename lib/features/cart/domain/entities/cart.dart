// ignore_for_file: public_member_api_docs, sort_constructors_first, must_be_immutable
import 'package:equatable/equatable.dart';
import 'package:warqkm/features/product/domain/entities/product.dart';

class Cart extends Equatable {
  Product product;
  double get totalPrice => quantity * product.price;
  int _quantity;

  Cart({required this.product, int quantity = 1}) : _quantity = quantity;

  int get quantity => _quantity;

  void increaseQuantity(int amount) {
    _quantity += amount;
  }

  void decreaseQuantity(int amount) {
    if (_quantity - amount >= 0) {
      _quantity -= amount;
    } else {
      throw Exception('Quantity cannot be negative');
    }
  }

  bool isValidQuantity(int quantity) {
    return quantity >= 1;
  }

  @override
  String toString() => 'Cart(product: $product, quantity: $quantity, totalPrice: $totalPrice)';

  @override
  List<Object> get props => [product, _quantity, totalPrice];
}
