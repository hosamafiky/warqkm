import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/data/cart_items.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/shared_widgets/custom_single_child_scroll_view.dart';
import 'package:warqkm/features/cart/presentation/widgets/cart_item_widget.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../../../core/themes/light/light_colors.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.cart_cart.tr())),
      body: CSingleChildScrollView(
        padding: EdgeInsets.all(24.asp),
        child: Column(
          children: [
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: cartItems.length,
              separatorBuilder: (context, index) {
                return CartItemWidget(cartItems[index]);
              },
              itemBuilder: (context, index) => 21.vsb,
            ),
            43.vsb,
            Text(
              LocaleKeys.cart_cart_hint.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displayMedium!.copyWith(
                    fontSize: 12.asp,
                    height: 1.6,
                    color: LightAppColors.dividerColor,
                  ),
            ),
            20.vsb,
            ElevatedButton(
              onPressed: () {},
              child: Text(LocaleKeys.cart_proceed_pay.tr()),
            ),
          ],
        ),
      ),
    );
  }
}
