import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/features/cart/domain/entities/cart.dart';
import 'package:warqkm/features/product/presentation/widgets/product_widget.dart';

import '../../../../translations/locale_keys.g.dart';

class CartItemWidget extends StatelessWidget {
  const CartItemWidget(
    this.cart, {
    super.key,
    this.imageBorderRadius,
    this.atPurchase = false,
  });

  final Cart cart;
  final bool atPurchase;
  final BorderRadiusGeometry? imageBorderRadius;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 114.aw,
      child: Row(
        children: [
          Stack(
            alignment: AlignmentDirectional.topEnd,
            children: [
              Container(
                width: 114.aw,
                alignment: AlignmentDirectional.topEnd,
                decoration: BoxDecoration(
                  borderRadius: imageBorderRadius ?? BorderRadius.circular(7.asp),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: CachedNetworkImageProvider(cart.product.image),
                  ),
                ),
              ),
              PositionedDirectional(
                end: 6.aw,
                top: 6.ah,
                child: RateWidget(rate: cart.product.rating.toInt()),
              ),
            ],
          ),
          18.hsb,
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  cart.product.name,
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        fontSize: 12.asp,
                        height: 1.3,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                ),
                Text(
                  cart.product.type,
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        fontSize: 11.asp,
                        height: 1,
                        color: LightAppColors.dividerColor,
                      ),
                ),
                Text(
                  'PDF (5,03) MB',
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        fontSize: 11.asp,
                        height: 1,
                        color: LightAppColors.dividerColor,
                      ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (cart.product.price == 0) ...[
                      Text(
                        'مجانًا',
                        style: Theme.of(context).textTheme.displaySmall!.copyWith(
                              height: 1.2,
                            ),
                      ),
                    ] else ...[
                      Text(
                        '${cart.product.price} ${LocaleKeys.currency_min_short.tr()}',
                        style: Theme.of(context).textTheme.displaySmall!.copyWith(
                              height: 1.2,
                            ),
                      ),
                    ],
                    if (!atPurchase) Image.asset('assets/images/biin.png', width: 17.aw, height: 17.aw),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
