// ignore_for_file: must_be_immutable

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/shared_widgets/custom_single_child_scroll_view.dart';
import 'package:warqkm/features/auth/presentation/pages/edit_profile_screen.dart';
import 'package:warqkm/features/auth/presentation/pages/login_screen.dart';
import 'package:warqkm/features/complaints/presentation/pages/send_complain_suggestion_screen.dart';
import 'package:warqkm/features/invoice/presentation/pages/invoices_screen.dart';
import 'package:warqkm/features/payment/presentation/pages/payment_cards_screen.dart';
import 'package:warqkm/features/product/presentation/pages/favorites_screen.dart';
import 'package:warqkm/features/providers/presentation/pages/service_providers_screen.dart';
import 'package:warqkm/features/settings/presentation/pages/settings_screen.dart';
import 'package:warqkm/features/wallet/presentation/pages/wallet_screen.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../../../core/shared_widgets/image_picker_widget.dart';

class MoreScreen extends StatelessWidget {
  const MoreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.layout_more.tr())),
      body: CSingleChildScrollView(
        child: Column(
          children: [
            16.vsb,
            ImagePickerWidget(isAbleToEdit: false),
            8.vsb,
            Text('حسام عابد', style: Theme.of(context).textTheme.bodySmall),
            19.vsb,
            MoreTiles(),
          ],
        ),
      ),
    );
  }
}

class MoreTiles extends StatelessWidget {
  MoreTiles({super.key});

  List<dynamic> tiles = [
    {
      'title': LocaleKeys.more_my_account.tr(),
      'icon': 'user',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, EditProfileScreen.routeName),
    },
    {
      'title': LocaleKeys.wallet_wallet.tr(),
      'icon': 'poket',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, WalletScreen.routeName),
    },
    {
      'title': LocaleKeys.invoices_invoices.tr(),
      'icon': 'purchese',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, InvoicesScreen.routeName),
    },
    {
      'title': LocaleKeys.payment_payment_cards.tr(),
      'icon': 'cards',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, PaymentCardsScreen.routeName),
    },
    {
      'title': LocaleKeys.serv_provider_providers.tr(),
      'icon': 'person',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, ServiceProvidersScreen.routeName),
    },
    {
      'title': LocaleKeys.more_be_provider.tr(),
      'icon': 'Group',
      'onTap': (BuildContext context) => {},
    },
    {
      'title': LocaleKeys.favorite_favorite.tr(),
      'icon': 'hearts',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, FavoritesScreen.routeName),
    },
    {
      'title': LocaleKeys.more_rate_us.tr(),
      'icon': 'starrrr',
      'onTap': (BuildContext context) => {},
    },
    {
      'title': LocaleKeys.more_contact_us.tr(),
      'icon': 'calll',
      'onTap': (BuildContext context) => {},
    },
    {
      'title': LocaleKeys.complaints_complaints_suggestions.tr(),
      'icon': 'feather-share',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, ComplaintSuggestionScreen.routeName),
    },
    {
      'title': LocaleKeys.more_share_app.tr(),
      'icon': 'share',
      'onTap': (BuildContext context) => {},
    },
    {
      'title': LocaleKeys.settings_settings.tr(),
      'icon': 'setting',
      'onTap': (BuildContext context) => Navigator.pushNamed(context, SettingsScreen.routeName),
    },
    {
      'title': LocaleKeys.more_about.tr(),
      'icon': 'information',
      'onTap': (BuildContext context) => {},
    },
    {
      'title': LocaleKeys.more_logout.tr(),
      'icon': 'exit',
      'onTap': (BuildContext context) => Navigator.pushReplacementNamed(context, LoginScreen.routeName),
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 31.aw),
      child: Column(
        children: tiles
            .map(
              (tile) => ListTile(
                minLeadingWidth: 28.aw,
                contentPadding: EdgeInsets.zero,
                onTap: () => tile['onTap'](context),
                leading: CircleAvatar(
                  backgroundColor: tiles.last == tile ? const Color(0xFFef3939) : Theme.of(context).cardColor,
                  radius: 16.aw,
                  child: Image.asset(
                    'assets/images/${tile['icon']}.png',
                    width: 16.aw,
                  ),
                ),
                title: Text(tile['title']),
                titleTextStyle: Theme.of(context).textTheme.bodySmall!.copyWith(color: const Color(0xFF193637)),
                trailing: const Icon(Icons.keyboard_arrow_left),
              ),
            )
            .toList(),
      ),
    );
  }
}
