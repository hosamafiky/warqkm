import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/shared_widgets/shimmer_loading_skeleton.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/features/product/domain/entities/product.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../pages/product_details_screen.dart';

class ProductWidget extends StatelessWidget {
  ProductWidget(
    this.product, {
    super.key,
    this.index = 0,
    this.isHorizontalList = false,
  }) : isInFavorite = ValueNotifier(product.isFavorite);

  final Product product;
  final int index;
  final bool isHorizontalList;
  final ValueNotifier<bool> isInFavorite;

  ProductWidget.forGrid(
    this.product, {
    super.key,
    this.index = 0,
  })  : isInFavorite = ValueNotifier(product.isFavorite),
        isHorizontalList = false;

  ProductWidget.forList(
    this.product, {
    super.key,
    this.index = 0,
  })  : isInFavorite = ValueNotifier(product.isFavorite),
        isHorizontalList = true;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.pushNamed(
        context,
        ProductDetailsScreen.routeName,
        arguments: {'product': product, 'index': index},
      ),
      child: !isHorizontalList
          ? Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 13.aw, vertical: 5.ah),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Hero(
                        tag: index,
                        child: CachedNetworkImage(
                          imageUrl: product.provider.image,
                          imageBuilder: (context, imageProvider) {
                            return CircleAvatar(
                              radius: 15.aw,
                              backgroundImage: CachedNetworkImageProvider(
                                product.provider.image,
                              ),
                            );
                          },
                          placeholder: (context, url) => ShimmerLoading(
                            width: 30.aw,
                            height: 30.aw,
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                      10.hsb,
                      Text(
                        product.provider.name,
                        style: Theme.of(context).textTheme.displayMedium!.copyWith(
                              color: Colors.black,
                              fontSize: 14.asp,
                              height: 1.1,
                            ),
                      ),
                    ],
                  ),
                ),
                7.vsb,
                Expanded(
                  child: Container(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7.asp),
                      border: Border.all(color: const Color(0xFFF3F3F3)),
                    ),
                    child: Stack(
                      fit: StackFit.expand,
                      children: [
                        Column(
                          children: [
                            Expanded(
                              flex: 117,
                              child: CachedNetworkImage(
                                imageUrl: product.image,
                                width: 100.w,
                                fit: BoxFit.cover,
                                placeholder: (context, url) => ShimmerLoading(
                                  width: 100.w,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 89,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 12.aw, vertical: 6.ah),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      product.name,
                                      style: Theme.of(context).textTheme.displayMedium!.copyWith(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 11.asp,
                                            height: 1.6,
                                          ),
                                    ),
                                    6.vsb,
                                    Text(
                                      product.type,
                                      style: Theme.of(context).textTheme.labelSmall!.copyWith(
                                            color: LightAppColors.grey7070.withOpacity(1),
                                            fontSize: 11.asp,
                                          ),
                                    ),
                                    const Spacer(),
                                    if (product.price == 0) ...[
                                      Text(
                                        'مجاني',
                                        style: Theme.of(context).textTheme.displaySmall!.copyWith(
                                              height: 1.2,
                                            ),
                                      ),
                                    ] else ...[
                                      Row(
                                        children: [
                                          Text(
                                            '${product.price} ${LocaleKeys.currency_min_short.tr()}',
                                            style: Theme.of(context).textTheme.displaySmall!.copyWith(
                                                  height: 1.2,
                                                ),
                                          ),
                                          if (product.price != product.oldPrice) ...[
                                            8.hsb,
                                            Text(
                                              '${product.oldPrice} ${LocaleKeys.currency_min_short.tr()}',
                                              style: Theme.of(context).textTheme.displaySmall!.copyWith(
                                                    decoration: TextDecoration.lineThrough,
                                                    color: LightAppColors.grey7070.withOpacity(1),
                                                    height: 1.2,
                                                  ),
                                            ),
                                          ],
                                        ],
                                      ),
                                    ],
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 37,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
                                  fixedSize: Size(100.w, 100.h),
                                ),
                                onPressed: () {},
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      LocaleKeys.product_add_to_cart.tr(),
                                      style: Theme.of(context).textTheme.displayMedium!.copyWith(
                                            color: Colors.white,
                                            fontSize: 12.asp,
                                            fontWeight: FontWeight.w400,
                                            height: 1.1,
                                          ),
                                    ),
                                    3.2.hsb,
                                    Image.asset(
                                      'assets/images/shopping-cart.png',
                                      width: 13.aw,
                                      height: 14.ah,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        PositionedDirectional(
                          top: 14.aw,
                          end: 14.aw,
                          child: RateWidget(rate: product.rating.toInt()),
                        ),
                        PositionedDirectional(
                          top: 14.aw,
                          start: 14.aw,
                          child: InkWell(
                            onTap: () => isInFavorite.value = !isInFavorite.value,
                            child: Container(
                              width: 32.aw,
                              height: 21.ah,
                              padding: EdgeInsets.symmetric(horizontal: 10.aw, vertical: 5.ah),
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.52),
                                borderRadius: BorderRadius.circular(4.asp),
                              ),
                              child: ValueListenableBuilder(
                                valueListenable: isInFavorite,
                                builder: (context, isFavorite, child) {
                                  return Image.asset(
                                    'assets/images/heart.png',
                                    color: isFavorite ? null : LightAppColors.grey7070.withOpacity(1),
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          : SizedBox(
              width: 147.aw,
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.asp),
                        border: Border.all(color: const Color(0xFFF3F3F3)),
                      ),
                      child: Stack(
                        fit: StackFit.expand,
                        children: [
                          Column(
                            children: [
                              Expanded(
                                flex: 117,
                                child: CachedNetworkImage(
                                  imageUrl: product.image,
                                  width: 100.w,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => ShimmerLoading(
                                    width: 100.w,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 89,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 7.aw, vertical: 6.ah),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        product.name,
                                        style: Theme.of(context).textTheme.displayMedium!.copyWith(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 11.asp,
                                              height: 1.6,
                                            ),
                                      ),
                                      const Spacer(),
                                      if (product.price == 0) ...[
                                        Text(
                                          'مجانًا',
                                          style: Theme.of(context).textTheme.displaySmall!.copyWith(
                                                height: 1.2,
                                              ),
                                        ),
                                      ] else ...[
                                        Row(
                                          children: [
                                            Text(
                                              '${product.price} ${LocaleKeys.currency_min_short.tr()}',
                                              style: Theme.of(context).textTheme.displaySmall!.copyWith(
                                                    height: 1.2,
                                                  ),
                                            ),
                                            if (product.price != product.oldPrice) ...[
                                              8.hsb,
                                              Text(
                                                '${product.oldPrice} ${LocaleKeys.currency_min_short.tr()}',
                                                style: Theme.of(context).textTheme.displaySmall!.copyWith(
                                                      decoration: TextDecoration.lineThrough,
                                                      color: LightAppColors.grey7070.withOpacity(1),
                                                      height: 1.2,
                                                    ),
                                              ),
                                            ],
                                          ],
                                        ),
                                      ],
                                    ],
                                  ),
                                ),
                              ),
                              Divider(height: 1.ah),
                              Expanded(
                                flex: 37,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 7.aw),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Hero(
                                        tag: index,
                                        child: CachedNetworkImage(
                                          imageUrl: product.provider.image,
                                          imageBuilder: (context, imageProvider) {
                                            return CircleAvatar(
                                              radius: (23 / 2).aw,
                                              backgroundImage: CachedNetworkImageProvider(
                                                product.provider.image,
                                              ),
                                            );
                                          },
                                          placeholder: (context, url) => ShimmerLoading(
                                            width: 23.aw,
                                            height: 23.aw,
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                      ),
                                      10.hsb,
                                      Text(
                                        product.provider.name,
                                        style: Theme.of(context).textTheme.displayMedium!.copyWith(
                                              color: Colors.black,
                                              fontSize: 14.asp,
                                              height: 1.1,
                                            ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          PositionedDirectional(
                            top: 14.aw,
                            end: 14.aw,
                            child: RateWidget(rate: product.rating.toInt()),
                          ),
                          PositionedDirectional(
                            top: 14.aw,
                            start: 14.aw,
                            child: InkWell(
                              onTap: () => isInFavorite.value = !isInFavorite.value,
                              child: Container(
                                width: 32.aw,
                                height: 21.ah,
                                padding: EdgeInsets.symmetric(horizontal: 10.aw, vertical: 5.ah),
                                decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.52),
                                  borderRadius: BorderRadius.circular(4.asp),
                                ),
                                child: ValueListenableBuilder(
                                  valueListenable: isInFavorite,
                                  builder: (context, isFavorite, child) {
                                    return Image.asset(
                                      'assets/images/heart.png',
                                      color: isFavorite ? null : LightAppColors.grey7070.withOpacity(1),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}

class RateWidget extends StatelessWidget {
  const RateWidget({
    Key? key,
    required this.rate,
  }) : super(key: key);

  final int rate;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4.asp),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.52),
        borderRadius: BorderRadius.circular(4.asp),
      ),
      child: Row(
        children: [
          Icon(Icons.star, size: 12.asp, color: LightAppColors.lightBlue),
          4.hsb,
          Text(
            rate.toString(),
            style: Theme.of(context).textTheme.displayMedium!.copyWith(
                  color: LightAppColors.lightBlue,
                  fontSize: 11.asp,
                  height: 1.1,
                ),
          ),
        ],
      ),
    );
  }
}
