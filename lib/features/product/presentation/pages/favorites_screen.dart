import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/data/products.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/features/product/presentation/widgets/product_widget.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class FavoritesScreen extends StatelessWidget {
  static const String routeName = '/favorites';
  const FavoritesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.favorite_favorite_list.tr()),
      ),
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (notification) {
          notification.disallowIndicator();
          return true;
        },
        child: GridView.builder(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 18.aw, vertical: 32.ah),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 165 / 280,
            crossAxisSpacing: 10.aw,
            mainAxisSpacing: 13.ah,
          ),
          itemCount: products.length,
          itemBuilder: (context, index) => ProductWidget.forGrid(products[index], index: index),
        ),
      ),
    );
  }
}
