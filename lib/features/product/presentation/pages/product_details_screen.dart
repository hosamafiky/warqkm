import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/features/product/domain/entities/product.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../../../core/data/products.dart';
import '../../../../core/themes/light/light_colors.dart';

class ProductDetailsScreen extends StatelessWidget {
  static const String routeName = '/product-details';
  const ProductDetailsScreen(this.product, {super.key, this.index = 0});

  final Product product;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.product_product_details.tr()),
        actions: [
          Image.asset('assets/images/share.png', width: 20.aw, height: 20.aw),
          25.hsb,
        ],
      ),
      body:  Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(17.asp),
              child: Column(
                children: [
                  SizedBox(
                    height: 64.aw,
                    child: Row(
                      children: [
                        Hero(
                          tag: products.map((e) => e.provider).toList().indexOf(product.provider),
                          child: Container(
                            width: 64.aw,
                            alignment: AlignmentDirectional.topEnd,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7.asp),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(product.provider.image),
                              ),
                            ),
                          ),
                        ),
                        18.hsb,
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                product.provider.name,
                                style: Theme.of(context).textTheme.displayMedium!.copyWith(
                                      fontSize: 12.asp,
                                      height: 1.3,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black,
                                    ),
                              ),
                              Text(
                                '500 ${LocaleKeys.follower.tr()}',
                                style: Theme.of(context).textTheme.displayMedium!.copyWith(
                                      fontSize: 11.asp,
                                      height: 1,
                                      color: LightAppColors.dividerColor,
                                    ),
                              ),
                              Text(
                                'PDF (5,03) MB',
                                style: Theme.of(context).textTheme.displayMedium!.copyWith(
                                      fontSize: 11.asp,
                                      height: 1,
                                      color: LightAppColors.dividerColor,
                                    ),
                              ),
                             ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
