// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/features/providers/domain/entities/service_provider.dart';

class Product extends Equatable {
  final ValueKey key = ValueKey(Random());
  final ServiceProvider provider;
  final String name;
  final String image;
  final double rating;
  final bool isFavorite;
  final String type;
  final double price;
  final double oldPrice;

   Product({
    required this.provider,
    required this.name,
    this.image = 'https://images.pexels.com/photos/4397913/pexels-photo-4397913.jpeg',
    required this.rating,
    required this.isFavorite,
    required this.type,
    required this.price,
    required this.oldPrice,
  });

  @override
  List<Object> get props {
    return [
      provider,
      name,
      image,
      rating,
      isFavorite,
      type,
      price,
      oldPrice,
    ];
  }
}
