import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/features/notifications/presentation/pages/notifications_screen.dart';

class NotificationsButton extends StatelessWidget {
  const NotificationsButton({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => Navigator.pushNamed(context, NotificationsScreen.routeName),
      icon: Image.asset('assets/images/noti.png', width: 20.aw, height: 20.aw),
    );
  }
}
