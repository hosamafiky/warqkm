import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class NotificationWidget extends StatelessWidget {
  const NotificationWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65.ah,
      padding: EdgeInsets.symmetric(horizontal: 10.aw, vertical: 12.ah),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.asp),
        boxShadow: const [
          BoxShadow(
            offset: Offset(0, 6),
            color: Colors.black12,
            blurRadius: 2,
          ),
          BoxShadow(
            offset: Offset(0, -6),
            color: Colors.black12,
            blurRadius: 2,
          ),
        ],
      ),
      child: IntrinsicHeight(
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(10.asp),
              decoration: BoxDecoration(
                color: Theme.of(context).cardColor.withOpacity(0.5),
                shape: BoxShape.circle,
              ),
              width: 40.aw,
              height: 40.aw,
              child: Image.asset('assets/images/bell.png'),
            ),
            12.hsb,
            Expanded(
              child: Column(
                children: [
                  Text(
                    LocaleKeys.auth_dummy_text.tr(),
                    maxLines: 1,
                    style: Theme.of(context).textTheme.displayMedium!.copyWith(
                          fontSize: 12.asp,
                          height: 1.3,
                        ),
                  ),
                  const Spacer(),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset('assets/images/clock.png', width: 11.aw, height: 11.aw),
                      3.73.hsb,
                      Text(
                        "${LocaleKeys.since.tr()} : 5 ${LocaleKeys.minutes.tr()}",
                        style: Theme.of(context).textTheme.displayMedium!.copyWith(
                              fontSize: 11.asp,
                              height: 1,
                              color: LightAppColors.dividerColor,
                            ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
