import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/features/notifications/presentation/widgets/notification_widget.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class NotificationsScreen extends StatelessWidget {
  static const String routeName = '/notifications';
  const NotificationsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.notifications_notifications.tr())),
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (notification) {
          notification.disallowIndicator();
          return true;
        },
        child: ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 16.aw, vertical: 25.ah),
          itemBuilder: (context, index) => const NotificationWidget(),
          separatorBuilder: (context, index) => 17.vsb,
          itemCount: 6,
        ),
      ),
    );
  }
}
