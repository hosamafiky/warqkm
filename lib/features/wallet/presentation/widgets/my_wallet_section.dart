import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class MyWalletSection extends StatelessWidget {
  const MyWalletSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          LocaleKeys.auth_dummy_text.tr(),
          maxLines: 1,
          overflow: TextOverflow.visible,
          style: Theme.of(context).textTheme.displayMedium,
        ),
        24.vsb,
        Image.asset('assets/images/pocket.png', height: 136.ah, width: 163.aw),
        15.vsb,
        RichText(
          text: TextSpan(
            text: '1,024\n',
            style: Theme.of(context).textTheme.displaySmall!.copyWith(fontSize: 40.asp, height: 1.2),
            children: [
              TextSpan(
                text: LocaleKeys.currency.tr(),
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium!
                    .copyWith(fontSize: 16.asp, height: 2.4, color: LightAppColors.grey7070.withOpacity(0.4)),
              ),
            ],
          ),
          textAlign: TextAlign.center,
        ),
        48.vsb,
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 14.aw),
          child: ElevatedButton(
            onPressed: () {},
            child: Text(LocaleKeys.wallet_balance_trans.tr()),
          ),
        ),
      ],
    );
  }
}
