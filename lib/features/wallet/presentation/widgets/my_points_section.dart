import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class MyPointsSection extends StatelessWidget {
  const MyPointsSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          LocaleKeys.auth_dummy_text.tr(),
          maxLines: 1,
          overflow: TextOverflow.visible,
          style: Theme.of(context).textTheme.displayMedium,
        ),
        8.vsb,
        Text(
          '${LocaleKeys.wallet_each.tr()} 200 ${LocaleKeys.wallet_point.tr()} = 100 ${LocaleKeys.currency_short.tr()}',
          style: Theme.of(context).textTheme.displaySmall!.copyWith(
                fontSize: 18.asp,
                fontWeight: FontWeight.w500,
              ),
        ),
        45.vsb,
        Image.asset('assets/images/medal.png', height: 125.ah, width: 106.aw),
        15.vsb,
        RichText(
          text: TextSpan(
            text: '1,024\n',
            style: Theme.of(context).textTheme.displaySmall!.copyWith(fontSize: 40.asp, height: 1.2),
            children: [
              TextSpan(
                text: LocaleKeys.wallet_point.tr(),
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium!
                    .copyWith(fontSize: 16.asp, height: 2.4, color: LightAppColors.grey7070.withOpacity(0.4)),
              ),
            ],
          ),
          textAlign: TextAlign.center,
        ),
        24.vsb,
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 14.aw),
          child: ElevatedButton(
            onPressed: () {},
            child: Text(LocaleKeys.wallet_conv_point_balance.tr()),
          ),
        ),
      ],
    );
  }
}
