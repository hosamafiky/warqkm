// ignore_for_file: must_be_immutable

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/features/wallet/presentation/widgets/my_points_section.dart';
import 'package:warqkm/features/wallet/presentation/widgets/my_wallet_section.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class WalletScreen extends StatelessWidget {
  static const String routeName = '/wallet';
  WalletScreen({super.key});

  List<String> tabs = [LocaleKeys.wallet_wallet.tr(), LocaleKeys.wallet_my_points.tr()];

  ValueNotifier<int> currentIndex = ValueNotifier(0);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: currentIndex.value,
      length: 2,
      child: ValueListenableBuilder(
        valueListenable: currentIndex,
        builder: (context, currentIndexValue, child) {
          return Scaffold(
            appBar: AppBar(title: Text(tabs[currentIndexValue])),
            body: Padding(
              padding: EdgeInsets.symmetric(vertical: 32.ah, horizontal: 16.aw),
              child: Column(
                children: [
                  TabBar(
                    onTap: (index) => currentIndex.value = index,
                    indicator: const BoxDecoration(),
                    labelPadding: EdgeInsets.zero,
                    tabs: tabs.indexed
                        .map((tab) => Container(
                              width: double.infinity,
                              margin: EdgeInsetsDirectional.only(
                                end: tab.$1 == 0 ? 5.aw : 0.0,
                                start: tab.$1 == 1 ? 5.aw : 0.0,
                              ),
                              height: 42.ah,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: tab.$1 == currentIndex.value ? Theme.of(context).primaryColor : LightAppColors.grey7070,
                                borderRadius: BorderRadius.circular(5.asp),
                              ),
                              child: Text(
                                tab.$2,
                                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                                      color: tab.$1 == currentIndex.value ? Colors.white : LightAppColors.textColor3,
                                      fontWeight: FontWeight.w500,
                                    ),
                              ),
                            ))
                        .toList(),
                  ),
                  32.vsb,
                  const Expanded(
                    child: TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        MyWalletSection(),
                        MyPointsSection(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
