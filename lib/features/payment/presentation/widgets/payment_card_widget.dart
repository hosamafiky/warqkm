import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';

import '../../domain/entities/card.dart';

class PaymentCardWidget extends StatelessWidget {
  const PaymentCardWidget(this.card, {super.key, this.index = 0});

  final PaymentCard card;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 160.ah,
      padding: EdgeInsets.symmetric(vertical: 33.ah, horizontal: 28.5.aw),
      decoration: BoxDecoration(
        gradient: index % 2 == 0
            ? const LinearGradient(
                colors: [
                  Color(0xFF35BB80),
                  Color(0xFF3D9B72),
                ],
              )
            : null,
        color: index % 2 == 1 ? LightAppColors.textColor3 : null,
        borderRadius: BorderRadius.circular(20.asp),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          const Spacer(),
          Image.asset('assets/images/sim.png', width: 40.aw, height: 30.ah),
          28.7.vsb,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset('assets/images/mastercard.png', width: 51.aw, height: 31.ah),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    card.cardNo.toString(),
                    style: Theme.of(context).textTheme.displayMedium!.copyWith(
                          height: 1.9,
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                  ),
                  8.hsb,
                  Text(
                    '*****',
                    style: Theme.of(context).textTheme.displayMedium!.copyWith(
                          height: 1.9,
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
