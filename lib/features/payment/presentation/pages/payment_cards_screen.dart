import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/features/payment/presentation/widgets/payment_card_widget.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../../../core/shared_widgets/custom_single_child_scroll_view.dart';
import '../../domain/entities/card.dart';

class PaymentCardsScreen extends StatelessWidget {
  static const String routeName = '/payment-cards';
  const PaymentCardsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.payment_payment_cards.tr()),
      ),
      body: CSingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 26.5.aw).copyWith(bottom: 31.ah),
        child: Column(
          children: [
            ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(vertical: 30.ah),
              shrinkWrap: true,
              itemCount: cards.length,
              separatorBuilder: (context, index) => PaymentCardWidget(cards[index], index: index),
              itemBuilder: (context, index) => 20.vsb,
            ),
            ElevatedButton(
              onPressed: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('assets/images/add.png', width: 17.aw, height: 17.aw),
                  8.hsb,
                  Text(LocaleKeys.payment_add_payment_card.tr()),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

List<PaymentCard> cards = List.filled(
  6,
  const PaymentCard(
    cardNo: 1234,
    expiryDate: '12/24',
    cvv: 992,
  ),
);
