// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class PaymentCard extends Equatable {
  final int cardNo;
  final String expiryDate;
  final int cvv;

  const PaymentCard({required this.cardNo, required this.expiryDate, required this.cvv});

  @override
  List<Object> get props => [cardNo, expiryDate, cvv];
}
