import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/shared_widgets/custom_single_child_scroll_view.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../../../core/shared_widgets/custom_text_form_field.dart';

class ComplaintSuggestionScreen extends StatefulWidget {
  static const String routeName = '/complaints';
  const ComplaintSuggestionScreen({super.key});

  @override
  State<ComplaintSuggestionScreen> createState() => _ComplaintSuggestionScreenState();
}

class _ComplaintSuggestionScreenState extends State<ComplaintSuggestionScreen> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _messageController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.complaints_complaints_suggestions.tr()),
      ),
      body: CSingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 18.aw, vertical: 28.ah),
        hasLayoutBuilder: true,
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                LocaleKeys.auth_name.tr(),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              11.34.vsb,
              CustomTextFormField(
                controller: _nameController,
                keyboardType: TextInputType.name,
                validator: (name) {
                  return name == null || name.isEmpty ? 'هذا الحقل مطلوب' : null;
                },
                hintText: LocaleKeys.auth_name_hint.tr(),
              ),
              14.vsb,
              Text(
                LocaleKeys.auth_email.tr(),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              11.34.vsb,
              CustomTextFormField(
                controller: _emailController,
                validator: (email) {
                  return email == null || email.isEmpty ? 'هذا الحقل مطلوب' : null;
                },
                keyboardType: TextInputType.emailAddress,
                hintText: LocaleKeys.auth_email_hint.tr(),
              ),
              14.vsb,
              Text(
                LocaleKeys.complaints_complaint_content.tr(),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              11.34.vsb,
              CustomTextFormField(
                controller: _messageController,
                keyboardType: TextInputType.text,
                validator: (message) {
                  return message == null || message.isEmpty ? 'هذا الحقل مطلوب' : null;
                },
                minLines: 8,
                hintText: LocaleKeys.complaints_complaint_content_hint.tr(),
              ),
              const Spacer(),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    debugPrint(_messageController.text);
                  }
                },
                child: Text(LocaleKeys.auth_send.tr()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
