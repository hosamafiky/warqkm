import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../../../../core/shared_widgets/custom_text_form_field.dart';

class SearchBarWidget extends StatelessWidget {
  const SearchBarWidget({super.key, required this.onChanged});

  final Function(String?) onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38.69.ah,
      margin: EdgeInsets.symmetric(horizontal: 16.aw),
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.all(10.asp),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(5.asp),
            ),
            child: Image.asset('assets/images/filter.png', width: 20.aw, height: 20.aw),
          ),
          10.hsb,
          Expanded(
            child: CustomTextFormField(
              onChanged: onChanged,
              contentPadding: EdgeInsets.all(11.5.asp),
              filled: true,
              fillColor: Colors.white,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(right: Radius.circular(5.asp)),
                borderSide: const BorderSide(color: LightAppColors.grey7070),
              ),
              hintText: LocaleKeys.home_search_bar_hint.tr(),
              hintStyle: TextStyle(
                fontSize: 13.asp,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.asp),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.horizontal(left: Radius.circular(5.asp)),
            ),
            child: Image.asset('assets/images/search.png', width: 20.aw, height: 20.aw),
          ),
        ],
      ),
    );
  }
}
