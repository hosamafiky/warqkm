import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/extensions/row.dart';

class MainSliderWidget extends StatefulWidget {
  const MainSliderWidget({super.key});

  @override
  State<MainSliderWidget> createState() => _MainSliderWidgetState();
}

class _MainSliderWidgetState extends State<MainSliderWidget> {
  ValueNotifier<int> currentIndexNotifier = ValueNotifier(0);
  late final PageController _pageController;
  late Timer timer;

  @override
  void initState() {
    _pageController = PageController(initialPage: currentIndexNotifier.value);
    timer = Timer.periodic(const Duration(seconds: 2), (_) {
      _pageController.nextPage(duration: const Duration(milliseconds: 300), curve: Curves.easeInOut);
    });
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      margin: EdgeInsets.symmetric(horizontal: 16.aw),
      height: 136.ah,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(9.asp),
      ),
      child: Stack(
        children: [
          SizedBox(
            width: 100.w,
            child: PageView.builder(
              controller: _pageController,
              onPageChanged: (index) => currentIndexNotifier.value = index,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: CachedNetworkImageProvider(sliderImages[index % sliderImages.length]),
                      fit: BoxFit.cover,
                    ),
                  ),
                );
              },
            ),
          ),
          Positioned(
            bottom: 8.ah,
            left: 0,
            right: 0,
            child: ValueListenableBuilder(
              valueListenable: currentIndexNotifier,
              builder: (context, index, child) {
                return SpacedRowExt.withSpacing(
                  spacing: 8.aw,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                    sliderImages.length,
                    (i) => Container(
                      width: 10.aw,
                      height: 10.aw,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: i == index % sliderImages.length ? Theme.of(context).primaryColor : Colors.white,
                        border: Border.all(color: Theme.of(context).primaryColor),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

List<String> sliderImages = [
  'https://img.freepik.com/free-vector/education-learning-concept-love-reading-people-reading-students-studying-preparing-examination-library-book-lovers-readers-modern-literature-flat-cartoon-vector-illustration_1150-60938.jpg',
  'https://cdn.todaysrdh.com/wp-content/uploads/2023/07/Emotional-Intelligence-3-Building-EQ.jpg',
];
