import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/data/products.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/shared_widgets/custom_single_child_scroll_view.dart';
import 'package:warqkm/features/home/presentation/widgets/search_bar.dart';
import 'package:warqkm/features/home/presentation/widgets/slider_widget.dart';
import 'package:warqkm/features/notifications/presentation/widgets/notifications_button.dart';
import 'package:warqkm/features/product/presentation/widgets/product_widget.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            title: Text(LocaleKeys.home_home.tr()),
            actions: [
              const NotificationsButton(),
              25.hsb,
            ],
          ),
          body: const Center(),
        ),
        Positioned.fill(
          top: 110.ah,
          left: 0,
          right: 0,
          child: CSingleChildScrollView(
            padding: EdgeInsets.only(bottom: 16.ah),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const MainSliderWidget(),
                10.vsb,
                SearchBarWidget(onChanged: (query) {}),
                16.5.vsb,
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.aw),
                  child: Text(LocaleKeys.home_secondary.tr()),
                ),
                10.vsb,
                SizedBox(
                  height: 220.ah,
                  child: ListView.separated(
                    shrinkWrap: true,
                    padding: EdgeInsets.symmetric(horizontal: 16.aw),
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return ProductWidget.forList(products[index], index: index);
                    },
                    separatorBuilder: (_, i) => 8.hsb,
                    itemCount: products.length,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
