import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';

import '../../domain/entities/service_provider.dart';

class ProviderWidget extends StatelessWidget {
  const ProviderWidget(this.provider, {super.key});

  final ServiceProvider provider;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Text(
            provider.name,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.displayMedium!.copyWith(
                  height: 1.1,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
          ),
        ),
        Expanded(
          child: Transform.flip(
            flipX: provider.isNotificationsOn,
            child: Image.asset(
              'assets/images/toggle.png',
              width: 23.aw,
              height: 11.ah,
              opacity: !provider.isNotificationsOn ? const AlwaysStoppedAnimation(0.2) : null,
              color: provider.isNotificationsOn ? Theme.of(context).primaryColor : null,
            ),
          ),
        ),
        Expanded(
          child: Image.asset(
            'assets/images/deleteee.png',
            width: 16.aw,
            height: 16.aw,
          ),
        ),
      ],
    );
  }
}
