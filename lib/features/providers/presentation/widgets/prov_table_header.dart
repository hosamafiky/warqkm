import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

class ProvidersTableHeader extends StatelessWidget {
  const ProvidersTableHeader({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 21.aw, vertical: 9.5.ah),
      decoration: BoxDecoration(color: LightAppColors.greyF5F5, borderRadius: BorderRadius.vertical(top: Radius.circular(5.asp))),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Text(
              LocaleKeys.serv_provider_provider_name.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displayMedium!.copyWith(
                    height: 1.1,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                  ),
            ),
          ),
          Expanded(
            child: Text(
              LocaleKeys.notifications.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displayMedium!.copyWith(
                    height: 1.1,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                  ),
            ),
          ),
          Expanded(
            child: Text(
              LocaleKeys.delete.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displayMedium!.copyWith(
                    height: 1.1,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
