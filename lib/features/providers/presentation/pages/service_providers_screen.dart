import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/shared_widgets/custom_single_child_scroll_view.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/features/providers/domain/entities/service_provider.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../widgets/prov_table_header.dart';
import '../widgets/provider_widget.dart';

class ServiceProvidersScreen extends StatelessWidget {
  static const String routeName = '/service-providers';
  const ServiceProvidersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.serv_provider_providers.tr())),
      body: CSingleChildScrollView(
        padding: EdgeInsets.symmetric(
          horizontal: 17.aw,
          vertical: 31.ah,
        ),
        child: Column(
          children: [
            const ProvidersTableHeader(),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 21.aw).copyWith(bottom: 23.ah, top: 9.ah),
              decoration: BoxDecoration(
                border: Border.all(color: LightAppColors.grey7070),
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(5.asp)),
              ),
              child: ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: providers.length,
                separatorBuilder: (context, index) => const Divider(),
                itemBuilder: (context, index) => ProviderWidget(providers[index]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

List<ServiceProvider> providers = List.generate(25, (index) => ServiceProvider(name: 'حسام عابد', isNotificationsOn: index % 2 == 0));
