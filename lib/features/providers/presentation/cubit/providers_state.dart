part of 'providers_cubit.dart';

abstract class ProvidersState extends Equatable {
  const ProvidersState();

  @override
  List<Object> get props => [];
}

class ProvidersInitial extends ProvidersState {}
