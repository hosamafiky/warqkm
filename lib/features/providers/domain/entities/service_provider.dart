// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class ServiceProvider extends Equatable {
  final String name;
  final String image;
  final bool isFollowed;
  final bool isNotificationsOn;

  const ServiceProvider({
    required this.name,
    this.image = defaultImageUrl,
    this.isFollowed = false,
    this.isNotificationsOn = false,
  });

  static const String defaultImageUrl = 'https://images.pexels.com/photos/91227/pexels-photo-91227.jpeg';

  @override
  List<Object> get props => [name, image, isNotificationsOn, isFollowed];
}
