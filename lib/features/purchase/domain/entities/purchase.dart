import 'package:equatable/equatable.dart';

import '../../../cart/domain/entities/cart.dart';

class Purchase extends Equatable {
  final Cart _cart;

  const Purchase({required Cart cart}) : _cart = cart;

  Cart get cart => _cart;

  @override
  String toString() => 'Purchase(cart: $cart)';

  @override
  List<Object> get props => [_cart];
}
