import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:warqkm/core/extensions/res_size.dart';

import '../../../../core/shared_widgets/custom_elevated_button.dart';
import '../../../../core/themes/light/light_colors.dart';
import '../../../../translations/locale_keys.g.dart';
import '../../../cart/presentation/widgets/cart_item_widget.dart';
import '../../../providers/domain/entities/service_provider.dart';
import '../../domain/entities/purchase.dart';

class PurchaseWidget extends StatelessWidget {
  const PurchaseWidget(this.purchase, {super.key, required this.onDownloadPressed, required this.onRatePressed});

  final Purchase purchase;
  final VoidCallback onDownloadPressed;
  final VoidCallback onRatePressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _ProviderHeader(provider: purchase.cart.product.provider),
        8.vsb,
        CartItemWidget(
          purchase.cart,
          atPurchase: true,
          imageBorderRadius: BorderRadius.vertical(top: Radius.circular(7.asp)),
        ),
        _ButtonsSection(
          onDownloadPressed: onDownloadPressed,
          onRatePressed: onRatePressed,
        ),
      ],
    );
  }
}

class _ButtonsSection extends StatelessWidget {
  const _ButtonsSection({required this.onDownloadPressed, required this.onRatePressed});

  final VoidCallback onDownloadPressed;
  final VoidCallback onRatePressed;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: CustomElevatedButton(
            borderRadius: BorderRadiusDirectional.only(bottomStart: Radius.circular(8.asp)),
            fixedSize: Size.fromHeight(39.ah),
            textStyle: Theme.of(context).textTheme.displayMedium!.copyWith(
                  fontSize: 13.asp,
                  height: 1.3,
                  fontWeight: FontWeight.w500,
                  color: Theme.of(context).primaryColor,
                ),
            onPressed: onDownloadPressed,
            child: Text(LocaleKeys.purchases_download.tr()),
          ),
        ),
        1.hsb,
        Expanded(
          child: CustomElevatedButton(
            borderRadius: BorderRadiusDirectional.only(bottomEnd: Radius.circular(8.asp)),
            fixedSize: Size.fromHeight(39.ah),
            textStyle: Theme.of(context).textTheme.displayMedium!.copyWith(
                  fontSize: 13.asp,
                  height: 1.3,
                  fontWeight: FontWeight.w500,
                  color: Theme.of(context).primaryColor,
                ),
            backgroundColor: LightAppColors.lightBlue2,
            onPressed: onRatePressed,
            child: Text(LocaleKeys.purchases_rate.tr()),
          ),
        ),
      ],
    );
  }
}

class _ProviderHeader extends StatelessWidget {
  const _ProviderHeader({
    required this.provider,
  });

  final ServiceProvider provider;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Row(
            children: [
              CachedNetworkImage(
                imageUrl: provider.image,
                imageBuilder: (context, imageProvider) {
                  return CircleAvatar(
                    radius: 16.aw,
                    backgroundImage: CachedNetworkImageProvider(provider.image),
                  );
                },
              ),
              8.hsb,
              Text(provider.name),
            ],
          ),
        ),
        Expanded(
          flex: 3,
          child: Row(
            children: [
              Image.asset(
                'assets/images/${provider.isFollowed ? "person" : "star"}.png',
                width: 18.aw,
                height: 16.ah,
                color: Theme.of(context).primaryColor,
              ),
              7.hsb,
              Text(
                provider.isFollowed ? LocaleKeys.unfollow.tr() : LocaleKeys.follow.tr(),
                style: Theme.of(context).textTheme.displayMedium!.copyWith(
                      fontSize: 13.asp,
                      height: 1.3,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).primaryColor,
                    ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
