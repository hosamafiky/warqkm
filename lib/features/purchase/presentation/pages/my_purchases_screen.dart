import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:warqkm/core/data/purchases.dart';
import 'package:warqkm/core/extensions/res_size.dart';
import 'package:warqkm/core/extensions/row.dart';
import 'package:warqkm/core/shared_widgets/custom_elevated_button.dart';
import 'package:warqkm/core/shared_widgets/custom_text_form_field.dart';
import 'package:warqkm/core/themes/light/light_colors.dart';
import 'package:warqkm/translations/locale_keys.g.dart';

import '../widgets/purchase_widget.dart';

class MyPurchasesScreen extends StatelessWidget {
  const MyPurchasesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.purchases_purchases.tr())),
      body: ListView.separated(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 18.aw, vertical: 22.ah),
        itemCount: purchases.length,
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemBuilder: (context, index) {
          final purchase = purchases[index];
          return PurchaseWidget(
            purchase,
            onDownloadPressed: () {},
            onRatePressed: () {
              showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(71.asp)),
                ),
                builder: (context) => const RateProductSheet(),
              );
            },
          );
        },
      ),
    );
  }
}

class RateProductSheet extends StatefulWidget {
  const RateProductSheet({
    super.key,
  });

  @override
  State<RateProductSheet> createState() => _RateProductSheetState();
}

class _RateProductSheetState extends State<RateProductSheet> {
  final _rateMessageController = TextEditingController();

  @override
  void dispose() {
    _rateMessageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        width: 100.w,
        padding: EdgeInsets.symmetric(horizontal: 18.aw, vertical: 28.ah),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  LocaleKeys.purchases_add_rate.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        fontSize: 16.asp,
                        height: 1.3,
                        fontWeight: FontWeight.w500,
                        color:  LightAppColors.grey777,
                      ),
                ),
                18.vsb,
                Text(
                  LocaleKeys.purchases_rate_product.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        fontSize: 15.asp,
                        height: 1.3,
                        fontWeight: FontWeight.w600,
                        color: LightAppColors.grey292D,
                      ),
                ),
                12.vsb,
                100.w.hsb,
                SpacedRowExt.withSpacing(
                  spacing: 10.aw,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.filled(
                    5,
                    Image.asset(
                      "assets/images/star.png",
                      width: 26.aw,
                      height: 26.aw,
                      color: LightAppColors.enabledStarColor,
                    ),
                  ),
                ),
                21.vsb,
                CustomTextFormField(
                  controller: _rateMessageController,
                  hintText: LocaleKeys.purchases_add_comment_here.tr(),
                  labelText: LocaleKeys.purchases_your_rate.tr(),
                  maxLines: 4,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: LightAppColors.dividerColor),
                    borderRadius: BorderRadius.all(Radius.circular(8.asp)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: LightAppColors.dividerColor),
                    borderRadius: BorderRadius.all(Radius.circular(8.asp)),
                  ),
                ),
                41.vsb,
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 14.aw),
                  child: CustomElevatedButton(
                    onPressed: () {},
                    child:  Text(LocaleKeys.purchases_rate.tr()),
                  ),
                ),
              ],
            ),
            PositionedDirectional(
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Image.asset('assets/images/close.png', width: 24.aw, height: 24.aw),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
