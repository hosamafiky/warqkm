import 'package:flutter/material.dart';
import 'package:warqkm/features/auth/presentation/pages/code_activation_screen.dart';
import 'package:warqkm/features/auth/presentation/pages/forget_password_screen.dart';
import 'package:warqkm/features/auth/presentation/pages/login_screen.dart';
import 'package:warqkm/features/auth/presentation/pages/register_screen.dart';
import 'package:warqkm/features/auth/presentation/pages/reset_password_screen.dart';
import 'package:warqkm/features/invoice/presentation/pages/invoices_screen.dart';
import 'package:warqkm/features/layout/presentation/pages/layout_screen.dart';
import 'package:warqkm/features/payment/presentation/pages/payment_cards_screen.dart';
import 'package:warqkm/features/product/presentation/pages/favorites_screen.dart';
import 'package:warqkm/features/providers/presentation/pages/service_providers_screen.dart';
import 'package:warqkm/features/wallet/presentation/pages/wallet_screen.dart';

import '../../features/auth/presentation/pages/edit_profile_screen.dart';
import '../../features/complaints/presentation/pages/send_complain_suggestion_screen.dart';
import '../../features/notifications/presentation/pages/notifications_screen.dart';
import '../../features/product/presentation/pages/product_details_screen.dart';
import '../../features/settings/presentation/pages/settings_screen.dart';

class RouterHelper {
  Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case LoginScreen.routeName:
        return MaterialPageRoute(builder: (context) => const LoginScreen());
      case RegisterScreen.routeName:
        return MaterialPageRoute(builder: (context) => const RegisterScreen());
      case ForgotPasswordScreen.routeName:
        return MaterialPageRoute(builder: (context) => const ForgotPasswordScreen());
      case ActivationCodeScreen.routeName:
        return MaterialPageRoute(builder: (context) => const ActivationCodeScreen());
      case ResetPasswordScreen.routeName:
        return MaterialPageRoute(builder: (context) => const ResetPasswordScreen());
      case LayoutScreen.routeName:
        return MaterialPageRoute(builder: (context) => const LayoutScreen());
      case EditProfileScreen.routeName:
        return MaterialPageRoute(builder: (context) => const EditProfileScreen());
      case WalletScreen.routeName:
        return MaterialPageRoute(builder: (context) => WalletScreen());
      case InvoicesScreen.routeName:
        return MaterialPageRoute(builder: (context) => const InvoicesScreen());
      case PaymentCardsScreen.routeName:
        return MaterialPageRoute(builder: (context) => const PaymentCardsScreen());
      case ServiceProvidersScreen.routeName:
        return MaterialPageRoute(builder: (context) => const ServiceProvidersScreen());
      case FavoritesScreen.routeName:
        return MaterialPageRoute(builder: (context) => const FavoritesScreen());
      case ComplaintSuggestionScreen.routeName:
        return MaterialPageRoute(builder: (context) => const ComplaintSuggestionScreen());
      case SettingsScreen.routeName:
        return MaterialPageRoute(builder: (context) => const SettingsScreen());
      case NotificationsScreen.routeName:
        return MaterialPageRoute(builder: (context) => const NotificationsScreen());
      case ProductDetailsScreen.routeName:
        final productData = settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(builder: (context) =>  ProductDetailsScreen(productData['product'], index: productData['index'],));
      default:
        return MaterialPageRoute(builder: (context) => _errorBuilder());
    }
  }
}

Widget _errorBuilder() => const Scaffold(
      body: Center(
        child: Text('Wrong Route'),
      ),
    );
