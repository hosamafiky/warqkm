enum Language {
  english(name: 'English', code: 'en', country: 'US'),
  arabic(name: 'اللغة العربية', code: 'ar', country: 'EG');

  const Language({
    required this.name,
    required this.code,
    required this.country,
  });

  final String name;
  final String code;
  final String country;
}
