// ignore_for_file: overridden_fields

import 'package:flutter/material.dart';

class CustomElevatedButton extends ElevatedButton {
  CustomElevatedButton({
    super.key,
    required this.onPressed,
    this.borderRadius,
    this.backgroundColor,
    this.fixedSize,
    this.textStyle,
    required this.child,
  }) : super(
          onPressed: onPressed,
          child: child,
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(borderRadius: borderRadius ?? BorderRadius.circular(10)),
            backgroundColor: backgroundColor,
            textStyle: textStyle,
            fixedSize: fixedSize,
          ),
        );

  @override
  final VoidCallback onPressed;
  @override
  final Widget? child;
  final BorderRadiusGeometry? borderRadius;
  final Color? backgroundColor;
  final Size? fixedSize;
  final TextStyle? textStyle;
}
