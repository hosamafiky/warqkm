// ignore_for_file: overridden_fields

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextFormField extends TextFormField {
  CustomTextFormField({
    super.key,
    this.controller,
    this.obscureText = false,
    this.filled = true,
    this.minLines,
    this.maxLines,
    this.hintText,
    this.labelText,
    this.hintStyle,
    this.validator,
    this.fillColor,
    this.suffix,
    this.keyboardType,
    this.contentPadding,
    this.inputFormatters = const [],
    this.focusedBorder,
    this.enabledBorder,
    this.onChanged,
    this.onSaved,
    this.textAlign = TextAlign.start,
  }) : super(
          onTapOutside: (_) => FocusManager.instance.primaryFocus?.unfocus(),
          controller: controller,
          keyboardType: keyboardType,
          obscureText: obscureText,
          validator: validator,
          onChanged: onChanged,
          minLines: minLines,
          maxLines: maxLines ?? minLines ?? 1,
          onSaved: onSaved,
          inputFormatters: inputFormatters,
          textAlign: textAlign,
          decoration: InputDecoration(
            hintText: hintText,
            labelText: labelText,
            alignLabelWithHint: true,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            filled: filled,
            suffixIcon: suffix,
            fillColor: fillColor,
            hintStyle: hintStyle,
            contentPadding: contentPadding,
            focusedBorder: focusedBorder,
            enabledBorder: enabledBorder,
          ),
        );

  @override
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final EdgeInsetsGeometry? contentPadding;
  final int? minLines, maxLines;
  final TextAlign textAlign;
  final TextStyle? hintStyle;
  final List<TextInputFormatter> inputFormatters;
  final InputBorder? focusedBorder, enabledBorder;
  final String? hintText, labelText;
  // final
  final Widget? suffix;
  final Color? fillColor;
  @override
  final String? Function(String?)? validator;
  final Function(String?)? onChanged;
  @override
  final Function(String?)? onSaved;
  final bool obscureText, filled;
}
