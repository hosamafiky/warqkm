import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class CSingleChildScrollView extends StatelessWidget {
  const CSingleChildScrollView({
    super.key,
    this.scrollDirection = Axis.vertical,
    this.reverse = false,
    this.primary,
    this.padding,
    this.child,
    this.restorationId,
    this.physics,
    this.controller,
    this.minHeightDifference = 0.0,
    this.hasLayoutBuilder = false,
    this.dragStartBehavior = DragStartBehavior.start,
    this.clipBehavior = Clip.hardEdge,
    this.keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
  });

  final Axis scrollDirection;
  final bool reverse;
  final EdgeInsetsGeometry? padding;
  final bool? primary;
  final ScrollPhysics? physics;
  final ScrollController? controller;
  final Widget? child;
  final DragStartBehavior dragStartBehavior;
  final Clip clipBehavior;
  final String? restorationId;
  final ScrollViewKeyboardDismissBehavior keyboardDismissBehavior;
  final bool hasLayoutBuilder;
  final double minHeightDifference;

  @override
  Widget build(BuildContext context) {
    return hasLayoutBuilder
        ? LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                key: key,
                scrollDirection: scrollDirection,
                reverse: reverse,
                primary: primary,
                physics: physics,
                controller: controller,
                dragStartBehavior: dragStartBehavior,
                clipBehavior: clipBehavior,
                restorationId: restorationId,
                keyboardDismissBehavior: keyboardDismissBehavior,
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: constraints.maxHeight - minHeightDifference,
                  ),
                  child: IntrinsicHeight(
                    child: Padding(
                      padding: padding ?? EdgeInsets.zero,
                      child: child,
                    ),
                  ),
                ),
              );
            },
          )
        : SingleChildScrollView(
            key: key,
            scrollDirection: scrollDirection,
            reverse: reverse,
            padding: padding,
            primary: primary,
            physics: physics,
            controller: controller,
            dragStartBehavior: dragStartBehavior,
            clipBehavior: clipBehavior,
            restorationId: restorationId,
            keyboardDismissBehavior: keyboardDismissBehavior,
            child: child,
          );
  }
}
