import 'package:flutter/material.dart';

class ShimmerLoading extends StatefulWidget {
  const ShimmerLoading({
    this.width,
    this.height,
    this.shape = BoxShape.rectangle,
    super.key,
  });

  final double? width, height;
  final BoxShape shape;

  @override
  State<ShimmerLoading> createState() => _ShimmerLoadingState();
}

class _ShimmerLoadingState extends State<ShimmerLoading> with SingleTickerProviderStateMixin {
  late final AnimationController controller;
  late Animation<Color?> colorTween;

  @override
  void initState() {
    controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 2500))..repeat();
    colorTween = TweenSequence(<TweenSequenceItem<Color?>>[
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.grey.withOpacity(0.3), end: Colors.grey.withOpacity(0.8)),
        weight: 50,
      ),
      TweenSequenceItem(
        tween: ColorTween(end: Colors.grey.withOpacity(0.3), begin: Colors.grey.withOpacity(0.8)),
        weight: 50,
      ),
    ]).animate(controller);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        return Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            shape: widget.shape,
            color: colorTween.value,
          ),
        );
      },
    );
  }
}
