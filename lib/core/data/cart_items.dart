import 'dart:math';

import 'package:warqkm/features/cart/domain/entities/cart.dart';
import 'package:warqkm/features/providers/domain/entities/service_provider.dart';

import '../../features/product/domain/entities/product.dart';

List<Cart> cartItems = List<Cart>.generate(
  8,
  (index) => Cart(
    product: Product(
      provider: const ServiceProvider(name: 'حسام عابد'),
      name: "المرجع الكامل في التحكم الكهربائي الصناعي",
      rating: Random().nextInt(5).toDouble() + 1,
      isFavorite: index % 2 == 0,
      type: 'تقارير - PDF',
      price: index % 2 == 0 ? 0 : 150,
      oldPrice: index % 2 == 0
          ? 0
          : index == 5
              ? 150
              : 180,
    ),
  ),
);
