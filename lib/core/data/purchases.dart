import 'dart:math';

import 'package:warqkm/features/cart/domain/entities/cart.dart';
import 'package:warqkm/features/providers/domain/entities/service_provider.dart';

import '../../features/product/domain/entities/product.dart';
import '../../features/purchase/domain/entities/purchase.dart';

List<Purchase> purchases = List<Purchase>.generate(
  2,
  (index) => Purchase(
    cart: Cart(
      product: Product(
        provider: ServiceProvider(name: index % 2 == 0 ? 'حسام عابد' : 'حسام الفقي', isFollowed: index % 2 == 0),
        name: "المرجع الكامل في التحكم الكهربائي الصناعي",
        rating: Random().nextInt(5).toDouble() + 1,
        isFavorite: index % 2 == 0,
        type: 'تقارير - PDF',
        price: index % 2 == 0 ? 0 : 150,
        oldPrice: index % 2 == 0
            ? 0
            : index == 5
                ? 150
                : 180,
      ),
    ),
  ),
);
