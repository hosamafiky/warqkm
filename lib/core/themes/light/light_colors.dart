import 'package:flutter/material.dart';

class LightAppColors {
  static const Color splashBackgroundColor = Color(0xFFD9FFED);
  static const Color scaffoldBackgroundColor = Colors.white;
  static const Color appBarTitleColor = Colors.white;
  static const Color avatarColor = Color(0xFF81acae);
  static const Color primaryColor = Color(0xFF3D9B72);
  static const Color lightPrimaryColor = Color(0xFF2D7679);
  static const Color textFieldFillColor = Color(0x66EFEFEF);
  static const Color textFieldtextColor = Color(0xFF193637);
  static const Color grey7070 = Color(0x1A707070);
  static const Color greyF5F5 = Color(0xFFF5F5F5);
  static const Color grey777 = Color(0xFF777777);
  static const Color lightBlue = Color(0xFF2885bf);
  static const Color lightBlue2 = Color(0xFF2885BF);
  static const Color textColor1 = Color(0xFF202020);
  static const Color textColor2 = Color(0xFF505050);
  static const Color textColor3 = Color(0xFF292D32);
  static const Color enabledStarColor = Color(0xFFFFB300);
  static const Color nameTextColor = Color(0xFF191919);
  static const Color hintTextColor = Colors.grey;
  static const Color grey292D = Color(0xFF292D32);
  static const Color dividerColor = Color(0xFFBCBCBC);
}
